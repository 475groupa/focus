from django.db import models
from django.contrib.auth.models import User
from accounts.models import *

# Quiz models and related models: Question, PossibleAnswer, StudentQuiz, StudentAnswer


class Quiz(models.Model):
    quizName = models.CharField(max_length=50)
    creator = models.ForeignKey('auth.user', related_name='quiz', null=True)
    course = models.ForeignKey(Course, related_name="quiz_course", null=True)
    quizWorth = models.IntegerField(default=100, null=True, blank=True)
    timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)
    dueDay = models.DateField(null=True)
    
    def __str__(self):
        return "{0}".format(self.quizName)


class Question(models.Model):
    quiz = models.ForeignKey(Quiz)
    qNumber = models.IntegerField()
    qType = models.CharField(max_length=20)
    qWorth = models.IntegerField(default=10, null=True, blank=True)
    qText = models.CharField(max_length=2000, null=True)
    
    def __str__(self):
        return "{0} Question .No {1}".format(self.quiz.quizName, self.qNumber)


class PossibleAnswer(models.Model):
    question = models.ForeignKey(Question)
    answerText = models.CharField(max_length=1000, null=True)
    correctAnswer = models.BooleanField()
    
    def __str__(self):
        return "{0} Question .No {1} Answer {2}".format(self.question.quiz.quizName,
                                                        self.question.qNumber,
                                                        self.answerText)


class StudentQuiz(models.Model):
    quiz = models.ForeignKey(Quiz)
    student = models.ForeignKey('auth.user', related_name='student_quiz')
    quizDueTime = models.DateTimeField()
    timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)
    score = models.FloatField()
    completed = models.BooleanField(default=False)
    
    def __str__(self):
        return "Student Quiz {0}".format(self.quiz.quizName) 


class StudentAnswer(models.Model):
    studentQuiz = models.ForeignKey(StudentQuiz)
    answerText = models.CharField(max_length=1000, null=True)
    
    def __str__(self):
        return "Student Answer {0}".format(self.answerText)
