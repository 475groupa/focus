from django.shortcuts import render

from django.http import HttpResponse
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser

from django.http import Http404
from rest_framework.views import APIView

from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import filters
from rest_framework import generics

from .models import *
from .serializers import *


#############################
# class based view for Quiz # (Using generic class based views)
#############################

class QuizList(generics.ListCreateAPIView):
    """
    List all quizzes, or create a new quiz.
    """
    queryset = Quiz.objects.all()
    serializer_class = QuizSerializer
    #filter_backends = (filters.DjangoFilterBackend,)
    """
    List all possible answers with a filter: quizName or creator
    """
    filter_fields = ('quizName', 'creator')


class QuizDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    Retrieve, update or delete a quiz instance.
    """
    queryset = Quiz.objects.all()
    serializer_class = QuizSerializer


'''
class QuizList(APIView):
    """
    List all quizzes, or create a new quiz.
    """
    def get(self, request, format=None):
        quiz = Quiz.objects.all()
        serializer = QuizSerializer(quiz, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = QuizSerializer(data=request.DATA)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class QuizDetail(APIView):
    """
    Retrieve, update or delete a quiz instance.
    """
    def get_object(self, pk):
        try:
            return Quiz.objects.get(pk=pk)
        except Quiz.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        quiz = self.get_object(pk)
        serializer = QuizSerializer(quiz)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        quiz = self.get_object(pk)
        serializer = QuizSerializer(quiz, data=request.DATA)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        quiz = self.get_object(pk)
        quiz.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
'''

#################################
# class based view for Question # (Using generic class based views)
#################################


class QuestionList(generics.ListCreateAPIView):
    """
    List all questions, or create a new question.
    """
    queryset = Question.objects.all()
    serializer_class = QuestionSerializer
    #filter_backends = (filters.DjangoFilterBackend,)
    """
    List all question with a filter: quiz or qNumber or qType
    """
    filter_fields = ('quiz', 'qNumber', 'qType')


class QuestionDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    Retrieve, update or delete a question instance.
    """
    queryset = Question.objects.all()
    serializer_class = QuestionSerializer

'''
class QuestionList(APIView):
    """
    List all questions, or create a new question.
    """
    def get(self, request, format=None):
        question = Question.objects.all()
        serializer = QuestionSerializer(question, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = QuestionSerializer(data=request.DATA)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class QuestionDetail(APIView):
    """
    Retrieve, update or delete a question instance.
    """
    def get_object(self, pk):
        try:
            return Question.objects.get(pk=pk)
        except Question.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        question = self.get_object(pk)
        serializer = QuestionSerializer(question)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        question = self.get_object(pk)
        serializer = QuestionSerializer(question, data=request.DATA)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        question = self.get_object(pk)
        question.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
'''

#######################################
# class based view for PossibleAnswer # (Using generic class based views)
#######################################


class PossibleAnswerList(generics.ListCreateAPIView):
    """
    List all possible answers, or create a new possible answer.
    """
    queryset = PossibleAnswer.objects.all()
    serializer_class = PossibleAnswerSerializer
    #filter_backends = (filters.DjangoFilterBackend,)
    """
    List all possible answers with a filter: question or correctAnswer
    """
    filter_fields = ('question', 'correctAnswer')


class PossibleAnswerDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    Retrieve, update or delete a possible_answer instance.
    """
    queryset = PossibleAnswer.objects.all()
    serializer_class = PossibleAnswerSerializer

'''
class PossibleAnswerList(APIView):
    """
    List all possible answers, or create a new possible answer.
    """
    def get(self, request, format=None):
        possible_answer = PossibleAnswer.objects.all()
        serializer = PossibleAnswerSerializer(possible_answer, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = PossibleAnswerSerializer(data=request.DATA)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class PossibleAnswerDetail(APIView):
    """
    Retrieve, update or delete a possible_answer instance.
    """
    def get_object(self, pk):
        try:
            return PossibleAnswer.objects.get(pk=pk)
        except PossibleAnswer.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        possible_answer = self.get_object(pk)
        serializer = PossibleAnswerSerializer(possible_answer)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        possible_answer = self.get_object(pk)
        serializer = PossibleAnswerSerializer(possible_answer, data=request.DATA)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        possible_answer = self.get_object(pk)
        possible_answer.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
'''

#####################################
# class based view for Student Quiz #
#####################################


class StudentQuizList(generics.ListCreateAPIView):
    """
    List all student quizzes, or create a new student quiz.
    """
    queryset = StudentQuiz.objects.all()
    serializer_class = StudentQuizSerializer
    #filter_backends = (filters.DjangoFilterBackend,)
    """
    List all student quizzes with a filter: quiz, student, quizDueTime, score, completed
    """
    filter_fields = ('quiz', 'student', 'quizDueTime', 'score', 'completed')


class StudentQuizDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    Retrieve, update or delete a student quiz instance.
    """
    queryset = StudentQuiz.objects.all()
    serializer_class = StudentQuizSerializer
'''
class StudentQuizList(APIView):
    """
    List all student quizzes, or create a new student quiz.
    """
    def get(self, request, format=None):
        student_quiz = StudentQuiz.objects.all()
        serializer = StudentQuizSerializer(student_quiz, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = StudentQuizSerializer(data=request.DATA)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class StudentQuizDetail(APIView):
    """
    Retrieve, update or delete a student_quiz instance.
    """
    def get_object(self, pk):
        try:
            return StudentQuiz.objects.get(pk=pk)
        except StudentQuiz.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        student_quiz = self.get_object(pk)
        serializer = StudentQuizSerializer(student_quiz)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        student_quiz = self.get_object(pk)
        serializer = StudentQuizSerializer(student_quiz, data=request.DATA)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        student_quiz = self.get_object(pk)
        student_quiz.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
'''

#######################################
# class based view for Student Answer #
#######################################


class StudentAnswerList(generics.ListCreateAPIView):
    """
    List all student answer, or create a new student answer.
    """
    queryset = StudentAnswer.objects.all()
    serializer_class = StudentAnswerSerializer
    #filter_backends = (filters.DjangoFilterBackend,)
    """
    List all possible answers with a filter: studentQuiz
    """
    filter_fields = ('studentQuiz', )


class StudentAnswerDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    Retrieve, update or delete a student_answer instance.
    """
    queryset = StudentAnswer.objects.all()
    serializer_class = StudentAnswerSerializer

'''
class StudentAnswerList(APIView):
    """
    List all student answer, or create a new student answer.
    """
    def get(self, request, format=None):
        student_answer = StudentAnswer.objects.all()
        serializer = StudentAnswerSerializer(student_answer, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = StudentAnswerSerializer(data=request.DATA)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class StudentAnswerDetail(APIView):
    """
    Retrieve, update or delete a student_answer instance.
    """
    def get_object(self, pk):
        try:
            return StudentAnswer.objects.get(pk=pk)
        except StudentAnswer.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        student_answer = self.get_object(pk)
        serializer = StudentAnswerSerializer(student_answer)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        student_answer = self.get_object(pk)
        serializer = StudentAnswerSerializer(student_answer, data=request.DATA)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        student_answer = self.get_object(pk)
        student_answer.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
'''
