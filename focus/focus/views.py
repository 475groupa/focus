"""Focus app views.

views.py
Worked on by: Tom Dryer (Team 1, Group A)
"""

from django.http import HttpResponse

def example_endpoint(request):
    return HttpResponse('{"test": "success"}')
