"""Package init for the Focus app.

__init__.py
Worked on by: Tom Dryer (Team 1, Group A)
"""

# Support pymysql if it's installed
try:
    import pymysql
    pymysql.install_as_MySQLdb()
except ImportError:
    pass
