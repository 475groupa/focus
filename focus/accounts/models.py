"""Account management models.

models.py
Worked on by: Tom Dryer (Team 1, Group A)
"""

from django.contrib.auth.models import User, Group
from django.db import models
from django.core.mail import send_mail
from django.conf import settings
import random
import string
import logging
from smtplib import SMTPException
import socket


logger = logging.getLogger(__name__)


def gen_password(length=settings.RAND_PASSWD_LEN):
    """Generate a mobile-keyboard-friendly random password."""
    return ''.join(random.choice(string.ascii_lowercase + string.digits)
                   for _ in range(length))


def notify_user(user, subject, message):
    """Send an email to notification to a user.

    Assumes an SMTP server has been configured in Django.
    """
    if settings.NOTIFY_SEND_EMAILS:
        try:
            logger.info('Emailing notification to {}'.format(user.email))
            send_mail(subject, message, settings.NOTIFY_ORIGIN, [user.email])
        except (SMTPException, socket.error) as e:
            logger.warning('Failed to email notification to {}: {}'
                           .format(user.email, e))
    else:
        logger.info('Notification to {}: {}: {}'
                    .format(user, subject, message))


# These groups are create automatically by a fixture. Other modules can import
# these function to get access to the groups.


def get_instructor_group():
    """Return the instructor group instance."""
    return Group.objects.get(name='Instructors')


def get_student_group():
    """Return the student group instance."""
    return Group.objects.get(name='Students')


# We're using Django's built-in User model.
# The user's optional and possiblely non-unique "profile name" is stored in
# User.first_name, because demanding that everyone's name has a single
# structure is silly.


class CourseManager(models.Manager):

    def get_or_create_user(self, email):
        """Get or create a user by email address."""
        user = User.objects.filter(email=email).first()
        if user is None:
            password = gen_password()
            user = User.objects.create_user(email, email, password)
            get_student_group().user_set.add(user)
            notify_user(
                user, 'Focus: Student Account Created',
                'Welcome to Focus. Your login credentials are {} and {}.'
                .format(user.email, password)
            )
        return user

    def create_course(self, name, instructor, emails):
        """Create a new course."""
        # get the users to be enrolled, creating them when necessary
        enrolled_users = [self.get_or_create_user(email=email)
                          for email in emails]
        course = self.create(name=name, instructor=instructor)
        course.enrolled_users.add(*enrolled_users)


class Course(models.Model):
    """A course with a name, instructor, and enrolled users."""
    name = models.CharField(max_length=100, unique=True)
    instructor = models.ForeignKey('auth.user', related_name='instructor')
    enrolled_users = models.ManyToManyField('auth.user')

    objects = CourseManager()
