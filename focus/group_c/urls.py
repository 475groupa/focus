'''
TEAM 1 GROUP C
CREATED BY: Jack Yan, 2014-07-22
MODIFIED BY: Yi Ding, Jack Yan
LAST MODIFIED: 2014-07-27

TODO:

'''
from django.conf.urls import url, patterns, include
from rest_framework.urlpatterns import format_suffix_patterns
from group_c import views

urlpatterns = patterns(
	'',
	url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),

	url(r'^addqa/$', views.AddQuestionAnswer.as_view()),

	url(r'^questions/$', views.QuestionViewSet.as_view()),
	url(r'^questions/(?P<pk>[0-9]+)/$', views.QuestionDetailView.as_view()),
	url(r'^questiona/$', views.QuestionActiveViewSet.as_view()),

	url(r'^submissions/$', views.SubmissionViewSet.as_view()),
	#url(r'^submissions/(?P<pk>[0-9]+)/$', views.SubmissionDetailView.as_view()),

	url(r'^answers/$', views.AnswerViewSet.as_view()),
	#url(r'^answers/(?P<pk>[0-9]+)/$', views.AnswerDetailView.as_view()),
)
