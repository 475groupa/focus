'''
File: views.py
Author: Erick Costigan
Description: Views to create an interface between the serializers.py classes and the front end.  Handles requests and filters.
Date Last Modified: July 6, 2014
'''
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.shortcuts import render
from rest_framework import viewsets, filters, generics
from group_e.models import Attendance_Check, Attendance,Server_Time
from accounts.models import Course
from rest_framework.decorators import api_view
from group_e.serializers import Attendance_Check_Serializer, Attendance_Serializer,Server_Time_Serializer
from django.core import serializers
from django.http import HttpResponse


class AttStamp(APIView):

    def get(self, request, *args, **kw):
        arg1 = request.GET.get('class_id',None)
        #result = serializers.serialize("json", Attendance.objects.values('timestamp').distinct().filter(class_id__exact=arg1))
        response = Response(Attendance.objects.values('timestamp').distinct().filter(class_id__exact=arg1), status=status.HTTP_200_OK)
        return response

class Stats(APIView):

    def get(self, request, *args, **kw):
        #?arg1 = timestamp, ?arg2 = code, date time field = 013-11-20 02:34:51.759
        #datetime.datetime.strptime(s_date, '%Y%m%d')
        arg1 = request.GET.get('timestamp', None)
        arg2 = request.GET.get('status', None)
        arg3 = request.GET.get('class_id',None)

        #result = myClass.do_work()
        result = serializers.serialize("json", Attendance.objects.filter(status__exact=arg2).filter(timestamp__startswith=arg1).filter(class_id__exact=arg3))
        response = Response(result, status=status.HTTP_200_OK)
        return response

class Attendance_Populate(APIView):
    def get(self, request, *args, **kw):
        arg1 = request.GET.get('class_id',None)
        arg2 = request.GET.get('timestamp',None)
        #result = serializers.serialize("json", Attendance.objects.filter(status__exact=arg1))
        try:
            course = Course.objects.get(pk=arg1)
            student_ids = list(user.id for user in course.enrolled_users.all())
            for i in student_ids:
                attendance = Attendance(student_id= i, class_id = arg1, timestamp = arg2,status=0)
                attendance.save()
        except Course.DoesNotExist:
            response = Response("Course does not exist", status=status.HTTP_200_OK)
            return response
        response = Response("Course Exists, DB populated", status=status.HTTP_200_OK)
        return response

class Attendance_Check_ViewSet(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = Attendance_Check_Serializer
    queryset = Attendance_Check.objects.all()
    filter_backends = (filters.SearchFilter,)
    filter_fields = ['timestamp','class_id','expiration_time','code']
    search_fields=['timestamp']

class Attendance_Check_ViewAll(viewsets.ModelViewSet):
    serializer_class = Attendance_Check_Serializer
    queryset = Attendance_Check.objects.all()
    filter_backends = (filters.SearchFilter,)
    filter_fields = ['timestamp','class_id','expiration_time','code']
    search_fields=['timestamp']


class Attendance_ViewSet(viewsets.ModelViewSet):
    queryset = Attendance.objects.all()
    serializer_class = Attendance_Serializer
    #filter_backends = (filters.SearchFilter,)
    filter_fields = ['timestamp','student_id','class_id','status']
    #search_fields=['timestamp']


class Attendance_ViewAll(generics.RetrieveUpdateDestroyAPIView):
    queryset = Attendance.objects.all()
    serializer_class = Attendance_Serializer
    #filter_backends = (filters.SearchFilter)
    filter_fields = ['timestamp','student_id','class_id','status']
    #search_fields=['timestamp']

class Server_Time_ViewSet(viewsets.ModelViewSet):
    serializer_class = Server_Time_Serializer
    queryset = Server_Time.objects.all()

