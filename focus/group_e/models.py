'''
File: models.py
Author: Erick Costigan
Description: Model definitions for creating database schema
Date Last Modified: July 6, 2014
'''
from django.db import models
from accounts.models import Course	
import datetime
from django.utils import timezone


class Attendance_Check(models.Model):
	class_id = models.IntegerField() 
	timestamp = models.DateTimeField(auto_now=True)
	expiration_time = models.IntegerField()
	code = models.IntegerField()

class Attendance(models.Model):
	student_id = models.IntegerField() 
	class_id = models.IntegerField() 
	timestamp = models.DateTimeField()
	status = models.CharField(max_length=100)

class Server_Time(models.Model):
	dummy = models.DateTimeField(default=timezone.now(), editable = False)




