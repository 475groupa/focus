/**
 * `static/js/services/Accounts.js`
 *
 * Worked on by: David Yan (Team 1, Group A)
 *   2014-06-10: Created the Accounts controller as part of the AngularJS shell
 *   2014-06-20: Added controller for login page
 *   2014-06-30: Added controller for course creation page
 *   2014-07-02: Added controller for instructor creation page
 *   2014-07-06: Added controller for course page
 *   2014-07-09: Added controller for profile page
 *   2014-07-09: Added controller for index page
 *   2014-07-20: Added controller for nav
 *   2014-07-20: Updated change name so that the name is displayed for the user
 *   2014-07-26: Added common form error view
 *
 * Worked on by: Jamie To (Team 1, Group A)
 *   2014-07-20: Added change username method in Profile Ctrl
 */

(function() {

  // Helped rejection call back. Use as follows in a promise rejection:
  // `displayError($scope)`
  function displayError(scope, errorAttr) {
    errorAttr || (errorAttr = "error");
    return _.bind(function(rejection) {
      this[errorAttr] = rejection.data.detail;
    }, scope);
  }

  /**
   * The nav controller.
   */
  this.controller('NavCtrl', function($scope, User) {
    $scope.User = User;
  });

  /**
   * The index controller.
   *
   * @route '/'
   */
  this.controller('IndexCtrl', function($scope, User, Course) {
    $scope.invalidInput = true;

    function getCourses() {
      $scope.courses = Course.getCourses().$object;
    }

    $scope.create = function(name, emails) {
      $scope.error = false;
      Course.create(name, emails).then(getCourses, displayError($scope));
    }

    $scope.isInvalid = function(emails) {
      $scope.invalidInput = $scope.createForm.$invalid || _.isEmpty(emails);
    }

    User.isLoggedIn && getCourses();
  });

  /**
   * The login controller.
   *
   * @route '/login'
   */
  this.controller('LoginCtrl', function($scope, $location, User) {
    $scope.login = function(email, password) {
      $scope.error = false;
      User.login(email, password).then(function() {
        $location.path('/');
      }, displayError($scope));
    };
  });

  /**
   * The profile controller.
   *
   * @route '/profile'
   */
  this.controller('ProfileCtrl', function($scope, User) {
    $scope.name = User.session.name;

    $scope.changePassword = function(old_password, new_password) {
      $scope.pwd_err = false;
      User.changePassword(old_password, new_password).then(function() {
        // Success
      }, displayError($scope, "pwd_err"));
    }

    $scope.changeName = function(name) {
      $scope.name_err = false;
      User.changeName(name).then(function() {
        User.session.name = name;
      }, displayError($scope, "name_err"));
    }
  });

  /**
   * The course controller.
   *
   * @route '/course'
   */
  this.controller('CourseCtrl', function($scope, Course, course) {
    $scope.course = course;
    $scope.invalidInput = true;

    function update(emails, errorAttr) {
      Course.update(course.id, emails).then(function() {
        $scope.course.student_emails = emails;
      }, displayError($scope, errorAttr));
    }

    $scope.add = function(emails) {
      $scope.add_err = false;
      emails = _.union(emails, $scope.course.student_emails);
      update(emails, "add_err");
    };

    $scope.remove = function(email) {
      var emails = _.without($scope.course.student_emails, email);
      update(emails);
    }

    $scope.isInvalid = function(emails) {
      $scope.invalidInput = $scope.createForm.$invalid  || _.isEmpty(emails);
    }

  });

  /**
   * The instructor controller.
   *
   * @route '/create-course'
   */
  this.controller('InstructorCtrl', function($scope, Instructor, instructors) {
    $scope.instructors = instructors.instructors;

    $scope.create = function(email) {
      $scope.error = false;
      Instructor.create(email).then(function() {
        Instructor.getInstructors().then(function(data) {
          $scope.instructors = data.instructors;
        });
      }, displayError($scope));
    };
  });

  // A directive for displaying errors coming from API calls. Use as follows:
  // `<p form-error="error" ng-if="error" class="alert alert-danger"></p>`
  // where `$scope.error` contains the the rejection details
  this.directive('formError', function() {
    return {
      link: function(scope, elem, attr) {
        var details = scope[attr.formError]
          , err_tmpl = "You have entered {} invalid email(s)."
          , emails_err;

        if (!details) {
          return;
        }

        if (_.isObject(details)) {
          if (details.student_emails && _.isArray(details.student_emails)) {
            emails_err = details.student_emails.shift();
            if (_.isString(emails_err)) {
              details = "At least one student email is required.";
            } else {
              details = err_tmpl.replace("{}", _.size(emails_err));
            }
          } else {
            details = _(details).toArray().flatten().value();
          }
        }

        elem.text(details);
      }
    }
  });

}).call(angular.module('controllers'));
