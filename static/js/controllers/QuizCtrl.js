// Yue Li, yuel@sfu.ca
// Jul 27th, 2014

(function() {
  this.controller('GroupBCtrl', function($scope, course) {
  $scope.course = course;
  
  });
  
  this.controller('EditQuizCtrl', function($scope, $location, quiz, course, Quiz) {
	  $scope.course = course;
	  $scope.quiz = quiz;
	  $scope.edit = function(quiz) {
		console.log(quiz);
		Quiz.edit(quiz).then(function(data) {
			console.log(data);
			// $location.path("/course/" + course.id + "/quizzes");
		});
	  }
  });

  this.controller('CreateQuizCtrl', function($scope, $http, $location, Quiz){

      $scope.ifSame = function(var1, var2){
          return (var1 == var2);
      };

      $scope.postPossibleAnswer = function(possibleAnswer){
          Quiz.createPossibleAnswer(possibleAnswer).then(function(data){
              console.log("success", data)
              console.log("Possible Answer Created!")
          },function (){
              console.log("failure to create Possible Answer")
          });
      };

      $scope.makePossibleAnswer = function(questionID, answerText, correctAnswer){
          var possibleAnswer=JSON.stringify({
                  "question": questionID,
                  "answerText":answerText,
                  "correctAnswer": correctAnswer
              });
          $scope.postPossibleAnswer(possibleAnswer);
      };

      $scope.makeQuestion=function(quizID, qNum, qType, questionData){
          var question = JSON.stringify({
                  "quiz": quizID,
                  "qNumber": qNum,
                  "qType": qType,
                  "qWorth": questionData.worth,
                  "qText": questionData.text
          });
          Quiz.createQuestion(question).then(function(data){
              $scope.makePossibleAnswer( data.id, questionData.choice1.text, $scope.ifSame(questionData.answer, "choice1"));
              $scope.makePossibleAnswer( data.id, questionData.choice2.text, $scope.ifSame(questionData.answer, "choice2"));
              $scope.makePossibleAnswer( data.id, questionData.choice3.text, $scope.ifSame(questionData.answer, "choice3"));
              $scope.makePossibleAnswer( data.id, questionData.choice4.text, $scope.ifSame(questionData.answer, "choice4"));

          },function (){
              alert("failure to create Question"+questionData.text)
          });
      };

      Quiz.getQuiz().then(function(quiz_data){
          $scope.quizzes = quiz_data;
          $scope.getLastId = function(list){

              if(list.length == 0){
                  return 1;
              }else{
                  return list[list.length-1][0].id;
              }
          }

          Quiz.getQuestion().then(function(question_data){
              $scope.questions = question_data;
              $scope.create = function(quizData){
                      var nqName = quizData.name;
                      var nqTotal = quizData.total;
                      var nqDue = quizData.due;
                      var nQuiz=[];
                      nQuiz.push(nqName,1,nqTotal,nqDue);
                      var data = JSON.stringify({
                          "quizName": nqName,
                          "creator": 1,
                          "quizWorth": nqTotal,
                          "dueDay": nqDue
                      });

                  var lastQuizIndex = 0;
                  var lastQuiz = [];
                  if ($scope.quizzes){
                      for (var i=0;  i < $scope.quizzes.length; i++){
                          if ($scope.quizzes[i].id > lastQuizIndex){
                              lastQuizIndex = $scope.quizzes[i].id;
                              lastQuiz = $scope.quizzes[i];
                          }
                      }
                  }

			      Quiz.createQuiz(data).then(function(data){
                      console.log("success", data)
                      alert("Quiz Created!")
                      var currentQuizID = 0;
                      if (lastQuizIndex<=0){
                          currentQuizID = 1;
                      }else{
                          currentQuizID =lastQuiz.id + 1
                      }
                      console.log("Current Quiz ID ["+currentQuizID+"] : ")

                      var lastQuestionIndex = 0;
                      var lastQuestion = [];
                      if ($scope.questions){
                          for (var i=0;  i < $scope.questions.length; i++){
                              if ($scope.questions[i].id > lastQuestionIndex){
                                  lastQuestionIndex = $scope.questions[i].id;
                                  lastQuestion = $scope.questions[i];
                              }
                          }
                      }
                      $scope.makeQuestion( currentQuizID, "1", "MC",quizData.question1);
                      $scope.makeQuestion( currentQuizID, "2", "MC",quizData.question2);
                      $scope.makeQuestion( currentQuizID, "3", "MC",quizData.question3);
                      $scope.makeQuestion( currentQuizID, "4", "MC",quizData.question4);
                      $scope.makeQuestion( currentQuizID, "5", "MC",quizData.question5);

//                      $location.path("/course/" + $scope.course.id + "/quizzes");
                  },function (){
                      console.log("failure")
                  });
              }
          });
      });
  });

  this.controller('QuizCtrl', function($scope, $http, $location, Quiz){
    Quiz.getQuiz().then(function(quiz_data){
        $scope.quizzes = quiz_data;        
    },

      $scope.remove = function(quiz){
          var current_quiz = quiz;
          console.log(current_quiz);
          Quiz.removeQuiz(current_quiz)
              .then(function (data, index) {                        
					$scope.quizzes.splice(index, 1);
                    }, function (data) {
                        $scope.error = "An Error has occurred while deleting quiz!" ;
                        //$scope.loading = false;
                    });
      }
)
  });


  this.controller('DetailCtrl', function($scope, $routeParams, Quiz) {
          Quiz.getQuiz().then(function(quizzes){
              $scope.id = parseInt($routeParams.idx);
              for (var i = 0; i < quizzes.length; i++) {
                  if ($scope.id === quizzes[i].id) {
                        $scope.item = quizzes[i];
                  }
              }

              console.log(quizzes);
              console.log($scope.id);
          });
  });


     this.directive('bars', function ($parse) {
          return {
             restrict: 'E',
             replace: true,
             template: '<div id="chart"></div>',
             link: function (scope, element, attrs) {
               var data = attrs.data.split(','),
               chart = d3.select('#chart')
                 .append("div").attr("class", "chart")
                 .selectAll('div')
                 .data(data).enter()
                 .append("div")
                 .transition().ease("elastic")
                 .style("width", function(d) { return d + "%"; })
                 .text(function(d) { return d + "%"; });
             }
          };
     });
}).call(angular.module('controllers'));
