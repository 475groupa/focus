(function() {

  this.factory('GroupC', function(Restangular) {

    function testCall() {
      return Restangular.one('endpoint.json').get();
    }

    return {
      testCall: testCall
    };
  });

  this.factory('Question', function(Restangular) {
  	var endpoint = Restangular.all('api/group_c/question')

    function add(content) {
    	return endpoint.post({
    		content: content
    	});
    }

    function get() {
    	return endpoint.getList();
    }

    function getQuestion(id) {
    	return endpoint.get(id).then(function(data) {
    		return data;
    	});
    }

    function update(id, content) {
    	return endpoint.all(id).patch({
    		content: content
    	});
    }

    return {
    	add: add,
    	get: get,
    	getQuestion: getQuestion,
    	update: update
    }
  });

}).call(angular.module('services'));