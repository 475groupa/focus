/**
 * `static/js/services/Accounts.js`
 *
 * Worked on by: David Yan (Team 1, Group A)
 *   2014-06-10: Created the Accounts services as part of the AngularJS shell
 *   2014-06-20: Added base functionality for login and login
 *   2014-06-20: Fixed CSRFToken issue when logging out
 *   2014-06-30: Added base functionality for course creation
 *   2014-07-02: Added base functionality for instructor creation
 *   2014-07-02: Refactored the way login/logout works (now via event emission)
 *   2014-07-06: Augmented Course model with ability to fetch course data
 *   2014-07-06: Added ability to add/remove students to an existing course
 *   2014-07-09: Added base functionality for instructor creation
 *   2014-07-19: Added login/logout to navbar
 *   2014-07-20: Added endpoint for getting metadata about the current session
 *   2014-07-20: Fixed change name not doing anything
 *   2014-07-21: Made current course be stored
 *
 * Worked on by: Jamie To (Team 1, Group A)
 *   2014-07-20: Added changeUsername
 */

(function() {

  // The cookie key to query for the csrftoken Django sets
  var csrftoken_key = 'csrftoken';

  this.factory('User', function(Restangular, Cookie, $rootScope) {
    var endpoint = Restangular.all('accounts/session')
      , isLoggedIn = !!Cookie.get(csrftoken_key)
      , session = false;

    /**
     * Logs a user in.
     *
     * @param {String} email The email of the user.
     * @param {String} password The password of the user.
     * @returns {Promise} Returns the promise of the API hit.
     */
    function login(email, password) {
      return endpoint.post({
        email: email,
        password: password
      }).then(function(data) {
        $rootScope.$emit('$login');
        return data;
      });
    }

    /**
     * Logs the current user out.
     *
     * @returns {Promise} Returns the promise of the API hit.
     */
    function logout() {
      return endpoint.remove({}).then(function(data) {
        $rootScope.$emit('$logout');
        Cookie.remove(csrftoken_key);
        return data;
      });
    }

    /**
     * Changes the password of the current user.
     *
     * @returns {Promise} Returns the promise of the API hit.
     */
    function changePassword(old_password, new_password) {
      return endpoint.patch({
        old_password: old_password,
        new_password: new_password
      });
    }

    /**
     * Changes the username of the current user.
     *
     * @returns {Promise} Returns the promise of the API hit.
     */
    function changeName(name) {
      return endpoint.patch({
        name: name
      });
    }

    /**
     * Get metadata about the current session.
     *
     * @returns {Promise} Returns the promise of the API hit.
     */
    function getSession() {
      return endpoint.get('');
    }

    return {
      isLoggedIn: isLoggedIn,
      session: session,
      getSession: getSession,
      login: login,
      logout: logout,
      changePassword: changePassword,
      changeName: changeName
    };
  });

  this.factory('Course', function(Restangular, $q, $rootScope) {
    var endpoint = Restangular.all('accounts/courses')
      , course = false;

    /**
     * Fetches the course based on the course id.
     *
     * @returns {Promise} Returns the promise of the API hit.
     */
    function getCourse(id) {
      return endpoint.get(id).then(function(data) {
        $rootScope.$emit('$course', data);
        return data;
      }, $q.reject);
    }

    /**
     * Fetches the list of courses for the current user.
     *
     * @returns {Promise} Returns the promise of the API hit.
     */
    function getCourses() {
      return endpoint.getList();
    }

    /**
     * Creates a course.
     *
     * @param {String} name The name of the course.
     * @param {Array} emails The emails of the students to invite to the course.
     * @returns {Promise} Returns the promise of the API hit.
     */
    function create(name, emails) {
      return endpoint.post({
        name: name,
        student_emails: emails
      });
    }

    /**
     * Updates the students of a course.
     *
     * @param {String} id The id of the course.
     * @param {Array} emails The emails of the studentsof the course.
     * @returns {Promise} Returns the promise of the API hit.
     */
    function update(id, emails) {
      return endpoint.all(id).patch({
        student_emails: emails
      });
    }

    return {
      update: update,
      create: create,
      getCourse: getCourse,
      getCourses: getCourses,
      course: course
    };
  });

  this.factory('Instructor', function(Restangular) {
    var endpoint = Restangular.all('accounts/instructors');

    /**
     * Gets a list of all instructors.
     *
     * @returns {Promise} Returns the promise of the API hit.
     */
    function getInstructors() {
      return endpoint.get('');
    }

    /**
     * Creates an instructor account.
     *
     * @param {String} email The email of the instructor.
     * @returns {Promise} Returns the promise of the API hit.
     */
    function create(email) {
      return endpoint.post({
        email: email
      });
    }

    return {
      getInstructors: getInstructors,
      create: create
    };
  });

  this.service('Cookie', function() {
    var defaultOptions = {
      path: '/',
      expires: 9001
    };

    /**
     * Returns the value of a cookie identified by `key`.
     *
     * @param {String} key The key of the cookie.
     * @returns {*} Returns the value of the cookie.
     */
    function get(key) {
      return cookie.get(key);
    }

    /**
     * Checks to see if a cookie has been set.
     *
     * @param {String} key The key of the cookie.
     * @returns {Boolean} Returns whether or not a cookie exists.
     */
    function has(key) {
      return angular.isDefined(this.get(key));
    }

    /**
     * Deletes a cookie.
     *
     * @param {String} key The key of the cookie.
     * @returns {Model} Returns the current `Cookie` instance.
     */
    function remove(key) {
      cookie.remove(key);
      return this;
    }

    /**
     * Stores a cookie.
     *
     * @param {String} key The key of the cookie.
     * @param {String|Object} value The value of the cookie.
     * @param {Object} [options={}] Any additional parameters of storage.
     * @returns {Model} Returns the current `Cookie` instance.
     */
    function set(key, value, options) {
      options = _.extend(defaultOptions, options);
      cookie.set(key, angular.toJson(value), options);
      return this;
    }

    return {
      get: get,
      has: has,
      remove: remove,
      set: set
    };
  });

}).call(angular.module('services', []));
