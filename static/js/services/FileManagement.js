/* fileManagement.js

Description:File management service and factory for the angular JS single page application

Team 1 Group D

Created by: Derek Ho, 2014-07-03

Programmers: Derek Ho, Kelvin Lau

Changes:
2014-07-03  Added features to call the REST API for uploading
2014-07-05  Added features to call the REST API to delete files
2014-07-05  Added validation of file in size and file type by extension
2014-07-19  Added the courseID field when uploading files. 
2014-07-20  Added courseID when retreiving files from endpoint
2014-07-22  Changed to blacklist of file extensions instead of whitelist. 
2014-07-22  Added more blacklist of file extensions


Known bugs:
Certain files types are not sending the file.type and causing a bad request. example: json 
*/

(function() {

  this.factory('FileManager', function(Restangular) {
    var endpoint = Restangular.one('file_management/upload');

    function uploadFile(file, courseId) {
      var form = new FormData()
        , filesize = file.size
        , fname = file.name
        //Blacklist of file extensions 
        , exts = /(\.ade|\.adp|\.app|\.asp|\.bas|\.bat|\.cer|\.chm|\.cmd|\.com|\.cpl|\.crt|\.csh|\.der|\.exe|\.fxp|\.gadget|\.hlp|\.hta|\.inf|\.ins|\.isp|\.its|\.js|\.jse|\.ksh|\.lnk|\.mad|\.maf|\.mag|\.mam|\.maq|\.mar|\.mas|\.mat|\.mau|\.mav|\.maw|\.mda|\.mdb|\.mde|\.mdt|\.mdw|\.mdz|\.msc|\.msh|\.msh1|\.msh2|\.mshxml|\.msh1xml|\.msh2xml|\.msi|\.msp|\.mst|\.ops|\.pcd|\.pif|\.plg|\.prf|\.prg|\.pst|\.reg|\.scf|\.scr|\.sct|\.shb|\.shs|\.ps1|\.ps1xml|\.ps2|\.ps2xml|\.psc1|\.psc2|\.tmp|\.url|\.vb|\.vbe|\.vbs|\.vsmacros|\.vsw|\.ws|\.wsc|\.wsf|\.wsh|\.xnk)$/i;
  
      //Front end validaiton of file size and file type.
      if (exts.exec(file.name)) {
        alert("This file type is not supported");
      } else if (filesize > 41943040) { //40 Megabyte limit
        alert("File size is too large. Files must be less than 40MB in size")
      } else {
        form.append('file', file);
        form.append('filesize', file.size);
        form.append('filename', file.name);
        form.append('filetype', file.type);
        form.append('course_id', courseId);

        return endpoint.withHttpConfig({
          transformRequest: angular.identity
        }).customPOST(form, '', undefined, {
          'Content-Type': undefined
        });
      }

    }

    function getFiles(courseId) {
      return endpoint.getList(courseId);
    }

    function deleteFile(file) {
      var endpoint = Restangular.all('file_management/delete');

      return endpoint.customDELETE(file.id);
    }

    return {
      uploadFile: uploadFile,
      getFiles: getFiles,
      deleteFile: deleteFile
    };
  });

}).call(angular.module('services'));
