// Yue Li, yuel@sfu.ca
// Jul 27th, 2014
(function() {

  this.factory('Quiz', function($http, Restangular){
      var quiz_endpoint = Restangular.one("group_b/quiz/");
	  var question_endpoint = Restangular.one("group_b/question/");
      var possibleAnswer_endpoint = Restangular.all("group_b/possibleAnswer/");
      return {
          getQuiz: function(){		      
              return quiz_endpoint.getList("")
                  .then(function(result){
				      console.log("Initial Quiz set: ",result);
                      return result;
                  });
          },
          getQuestion: function(){
              return question_endpoint.get("")
                  .then(function(result){
				      console.log("Initial Question set: ",result);
                      return result.data;
                  });
          },
          removeQuiz: function (id) {
              return quiz_endpoint.customDELETE(id+"/");
          },
		  createQuiz: function (data){
              return quiz_endpoint.post("", data);
		  },
          createQuestion: function (data){
              return question_endpoint.customPOST(data);
		  },
          createPossibleAnswer: function (data){
              return possibleAnswer_endpoint.customPOST(data);
		  },
		  edit: function(quiz) {
			return quiz_endpoint.customPUT(quiz);
		  }
      }
  })

  this.factory('GroupB', function(Restangular) {

    function testCall() {
      return Restangular.one('endpoint.json').get();
    }

    return {
      testCall: testCall
    };
  });

}).call(angular.module('services'));
